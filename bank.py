


def logo_predict(model_root,image_root):
    ## Model use
    # make a prediction for a new image.
    from keras.preprocessing.image import load_img
    from keras.preprocessing.image import img_to_array
    from keras.models import load_model
    # modelx = load_model('/home/selva/Selva/Products/Task/Datas/model/logomdl1.h5')
    modelx = load_model(model_root)
    import cv2
    # class_names=['axis bank', 'icici bank', 'iob bank', 'sbi bank', 'standard chartered bank', 'yes bank']
    class_names=['aadharcard',  'pancard', 'passport']
    img_height=46
    img_width=200


    from keras.models import load_model
    from PIL import ImageTk, Image
    import numpy as np
    import matplotlib.pyplot as plt
    import keras
    import tensorflow as tf

    # file_path=r'/home/selva/Selva/Products/Task/Datas/ic1.png'
    file_path=image_root

    
    testimg=cv2.imread(file_path)

    img = keras.preprocessing.image.load_img(
        file_path, target_size=(img_height, img_width)
    )
    img_array = keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0)

    predictions = modelx.predict(img_array)
    score = tf.nn.softmax(predictions[0])

   

   
    # plt.imshow(testimg)
    # plt.title(str(format(class_names[np.argmax(score)])))
    # plt.show()
    out_string=str(format(class_names[np.argmax(score)]))
    return out_string


# model_root=r'/home/selva/Selva/Products/Task/Datas/model/logomdl1.h5'
# image_root=r'/home/selva/Selva/Products/Task/Datas/ic1.png'
# prd=logo_predict(model_root,image_root)
# print('The input image from', prd)